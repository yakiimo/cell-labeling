from cellpose import utils, io, models
from cellpose import plot

import matplotlib.pyplot as plt
import matplotlib as mpl

import numpy as np  
import matplotlib.pyplot as plt
  
def NcellContour(path):
    dat = np.load(path, allow_pickle=True).item()
    outlines = utils.outlines_list(dat['masks'])
    return len(outlines)

def drawCellContour(last_count):
    dat = np.load('L4_seg.npy', allow_pickle=True).item()
    img = io.imread('L4.tif')
    # plot image with masks overlaid
    mask_RGB = plot.mask_overlay(img, dat['masks'],
                            colors=np.array(dat['colors']))

    # plot image with outlines overlaid in red
    outlines = utils.outlines_list(dat['masks'])
    plt.imshow(img)
    for o in outlines:
        plt.plot(o[:,0], o[:,1], color='r')
    plt.show()

##matplotlib inline
#mpl.rcParams['figure.dpi'] = 300
#
## read image
#filename = 'L4.jpg'
#img = io.imread(filename)
#
## set model
#model = models.Cellpose(gpu=False, model_type='cyto')
#
## define CHANNELS to run segementation on
## grayscale=0, R=1, G=2, B=3
## channels = [cytoplasm, nucleus]
#chan = [2,3]
#
## segment image
#masks, flows, styles, diams = model.eval(img, diameter=None, channels=chan)
#
## save results so you can load in gui
#io.masks_flows_to_seg(img, masks, flows, diams, filename, chan)
#
## save results as png
#io.save_to_png(img, masks, flows, filename)
#
## display results
#fig = plt.figure(figsize=(12,5))
#plot.show_segmentation(fig, img, masks, flows[0], channels=chan)
#plt.tight_layout()
#plt.show()

if __name__ == '__main__':
    print('This file should not be run! Please run "interface.py".')
