import numpy as np
np.set_printoptions(threshold=np.inf)
import cv2

seg_path = 'L4_seg.npy'


def outlines_list(masks):
    outpix = []
    for n in np.unique(masks)[1:]:
        mn = masks == n
        if mn.sum() > 0:
            contours = cv2.findContours(mn.astype(np.uint8), mode=cv2.RETR_EXTERNAL,
                                        method=cv2.CHAIN_APPROX_NONE)
            contours = contours[-2]
            cmax = np.argmax([c.shape[0] for c in contours])
            pix = contours[cmax].astype(int).squeeze()
            if len(pix) > 4:
                outpix.append(pix)
            else:
                outpix.append(np.zeros((0, 2)))
    return outpix


dat = np.load(seg_path, allow_pickle=True).item()
dat = dat['outlines']
# outlines = outlines_list(dat['masks'])
with open('test.txt','w') as f:
    f.write(str(dat))
    f.close()