// 需要输入的变量
const cell_in_pic = "170多";
var img_url = './src/L4.jpg';
var json_url = './src/L4.json';

// Written by Yakiimo
//
// 加载图片
loadPicture(img_url);
document.getElementById('cell_num').innerHTML = cell_in_pic;

// 加载json文件
var imageMatrix;
fetch(json_url)
    .then(response => response.json())
    .then(data => {
        imageMatrix = data;
        drawContour(imageMatrix); //绘制边缘图
        listenLabel(imageMatrix); //等待标记
    })
    .catch(error => {
        console.error('Error while loading json:', error);
    });

// 下面是 监听按钮
// 切换轮廓显示
document.getElementById('show_contour').addEventListener('click', function() {
    if (this.checked) {
        displayContour(1);
    } else {
        displayContour(0);
    }
});
// 标记异常细胞
document.getElementById('label_unormal').addEventListener('click', function() {
    let ctx = document.getElementById('contour_canva').getContext('2d');
    drawOneContour(imageMatrix, selectedOutline, ctx, 'red');
    document.getElementById('label_log').innerHTML += "已标记"+selectedOutline+"号细胞<br>"
});
// 标记不是细胞
document.getElementById('label_error').addEventListener('click', function() {
    let ctx = document.getElementById('contour_canva').getContext('2d');
    drawOneContour(imageMatrix, selectedOutline, ctx, 'white');
});
//  取消标记
document.getElementById('label_cancel').addEventListener('click', function() {
    let ctx = document.getElementById('contour_canva').getContext('2d');
    drawOneContour(imageMatrix, selectedOutline, ctx, 'black');
    document.getElementById('label_log').innerHTML += "取消标记"+selectedOutline+"号细胞<br>"
});
//
//
//

function drawContour(imageMatrix) {
    let canvas = document.getElementById('contour_canva');
    let ctx = canvas.getContext('2d');
    for (let i = 0; i < imageMatrix.length; i++) {
        for (let j = 0; j < imageMatrix[i].length; j++) {
            if (imageMatrix[i][j] === 0) {
                continue;
            } else {
                ctx.fillStyle = 'black'; // 如果值不为0,填色
            }
            ctx.fillRect(j, i, 2, 2);
        }
    }
}

function loadPicture(img_url) {
    var container = document.getElementById('image_container');
    var img = new Image();
    img.src = img_url;
    img.onload = function() {
        container.appendChild(img);
    };
    img.onerror = function() {
        alert('无法加载图片');
    };
}

function listenLabel(imageMatrix) {
    // 定义图像矩阵
    var canvas = document.getElementById('label_canva');
    var ctx = canvas.getContext('2d');
    canvas.addEventListener('click', function(event) {
        let rect = this.getBoundingClientRect();
        let x = Math.floor((event.clientX - rect.left)/0.7);
        let y = Math.floor((event.clientY - rect.top)/0.7);
        for (let i = 0; i < 768; i++) {
            if (imageMatrix[y][x-i] != 0) {
                selectedOutline = imageMatrix[y][x-i];
                ctx.clearRect(0,0,canvas.width,canvas.height)
                drawOneContour(imageMatrix, selectedOutline, ctx, 'red');
                break
                }
        }
    });
};

function displayContour(is_checked) {
    var element = document.getElementById('contour_canva');
    if (is_checked) {
        element.style.display = 'block';
    } else {
        element.style.display = 'none';
    }
}

function drawOneContour(imageMatrix, selectedOutline, ctx, color) {
    for (let row = 0; row < imageMatrix.length; row++) {
        for (let col = 0; col < imageMatrix[row].length; col++) {
            if (imageMatrix[row][col] === selectedOutline) {
                ctx.fillStyle = color;
                ctx.fillRect(col,row,2,2)
            }
        }
    }
}