import numpy as np 
import cv2

np.set_printoptions(threshold=np.inf)

def outlines_list(masks):
    outpix = []
    for n in np.unique(masks)[1:]:
        mn = masks == n
        if mn.sum() > 0:
            contours = cv2.findContours(mn.astype(np.uint8), mode=cv2.RETR_EXTERNAL,
                                        method=cv2.CHAIN_APPROX_NONE)
            contours = contours[-2]
            cmax = np.argmax([c.shape[0] for c in contours])
            pix = contours[cmax].astype(int).squeeze()
            if len(pix) > 4:
                outpix.append(pix)
            else:
                outpix.append(np.zeros((0, 2)))
    return outpix
seg_path = 'L4.npy'
dat = np.load(seg_path, allow_pickle=True).item()
print(dat['masks'][0,0])
#outlines = outlines_list(dat.items())
'''
all_outlines = 'all_outlines.txt'
with open(all_outlines, 'w') as f:
    f.write(str(dat['masks']))
'''