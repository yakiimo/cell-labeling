import numpy as np
import json

# 读取npy文件
def read_npy(file_name):
    return np.load(file_name,allow_pickle=True).item()

f = read_npy('./db/L4.npy')
matrix = f['outlines']
matrix_list = matrix.tolist()

with open('./db/L4.json', 'w') as file:
    json.dump(matrix_list, file)