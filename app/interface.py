import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
import io
import os

from PIL import Image, ImageTk
import matplotlib.pyplot as plt
import numpy as np
import cv2
 
 ##########
image_files = [] #用于记录文件夹下所有的图片文件路径信息
contour_files = []
image_number = 0 #用于记录当前显示的图片序号
continues_folder_selected = ''
cell_number = []
photo_path = ''
seg_path = ''
fig_width = 600 
fig_height = 450
img_width = 0
img_height = 0
noting_file = open('noting.dat','w+')

#标注所有轮廓线
def drawCellContour(outlines): 
    global cell_type  
    plt.figure()
    plt.axis('off')
    img = Image.open(photo_path)
    plt.imshow(img)
    for i in range(len(outlines)):
        if cell_type[0][i-1] ==1:
            plt.plot(outlines[i-1][:,0], outlines[i-1][:,1], color='y')
        elif cell_type[0][i-1] == 2:
            plt.plot(outlines[i-1][:,0], outlines[i-1][:,1], color='r')   
    img_buf = io.BytesIO()
    plt.savefig(img_buf, format='png',bbox_inches='tight',pad_inches=0)
    plt.cla()  
    global im
    im = Image.open(img_buf)
    im = im.resize((fig_width,fig_height))
    im = ImageTk.PhotoImage(im)
    cell_photo.config(image=im)
    cell_photo.update()


#显示标注功能
def on_checkbutton_change(): 
    global im, cell_photo, cell_type, im 
    if photo_path == '' or seg_path == '':
        messagebox.showwarning("警告", "还未选择正确的图片文件和轮廓文件")
        return
    else:
        if checkbutton_showOutline.get() == 1:  
            drawCellContour(outlines)
        else:
            plt.figure()
            plt.axis('off')
            img = Image.open(photo_path)
            plt.imshow(img)
            for i in range(len(outlines)):
                if cell_number is not None and i == cell_number:
                    plt.plot(outlines[i-1][:,0], outlines[i-1][:,1], color='k')
            img_buf = io.BytesIO()
            plt.savefig(img_buf, format='png',bbox_inches='tight',pad_inches=0)
            plt.cla()  
            im = Image.open(img_buf)
            im = im.resize((fig_width,fig_height))
            im = ImageTk.PhotoImage(im)
            cell_photo.config(image=im)
            cell_photo.update()


#显示选中细胞
def label_cell():  
    if photo_path == '' or seg_path == '':
        messagebox.showwarning("警告", "还未选择正确的图片文件和轮廓文件")
        return
    elif cell_number is not None and cell_number <= len(outlines):
        global im, cell_photo
        if checkbutton_showOutline.get() == 1: 
            plt.figure()
            plt.axis('off')
            img = Image.open(photo_path)
            plt.imshow(img)
            for i in range(len(outlines)):
                if i == cell_number:
                    plt.plot(outlines[i-1][:,0], outlines[i-1][:,1], color='k')
                elif cell_type[0][i-1] ==1:
                    plt.plot(outlines[i-1][:,0], outlines[i-1][:,1], color='y')
                elif cell_type[0][i-1] == 2:
                    plt.plot(outlines[i-1][:,0], outlines[i-1][:,1], color='r')     
            img_buf = io.BytesIO()
            plt.savefig(img_buf, format='png',bbox_inches='tight',pad_inches=0)
            plt.cla()  
            im = Image.open(img_buf)
            im = im.resize((fig_width,fig_height))
            im = ImageTk.PhotoImage(im)
            cell_photo.config(image=im)
            cell_photo.update()
        else:
            plt.figure()
            plt.axis('off')
            img = Image.open(photo_path)
            plt.imshow(img)
            for i in range(len(outlines)):
                if i == cell_number:
                    plt.plot(outlines[i-1][:,0], outlines[i-1][:,1], color='k')
            img_buf = io.BytesIO()
            plt.savefig(img_buf, format='png',bbox_inches='tight',pad_inches=0)
            plt.cla()  
            im = Image.open(img_buf)
            im = im.resize((fig_width,fig_height))
            im = ImageTk.PhotoImage(im)
            cell_photo.config(image=im)
            cell_photo.update()
    else:
        messagebox.showwarning("错误", "请选择一个细胞")


#关闭窗口
def close_window():
    os._exit(0)


#图片大小随窗口宽度变化
def resize_image(event):
    fig_width = int(event.width*2/3)
    fig_height = int(fig_width*3/4)
    if photo_path == '':
        image = create_white_image(fig_width,fig_height)
    else:
        image = Image.open(photo_path)  
    img = image.resize((fig_width, fig_height)) 
    im = ImageTk.PhotoImage(img)
    cell_photo.config(image=im)
    cell_photo.update()


#通过masks获得轮廓数据
def outlines_list(masks):
    outpix = []
    for n in np.unique(masks)[1:]:
        mn = masks == n
        if mn.sum() > 0:
            contours = cv2.findContours(mn.astype(np.uint8), mode=cv2.RETR_EXTERNAL,
                                        method=cv2.CHAIN_APPROX_NONE)
            contours = contours[-2]
            cmax = np.argmax([c.shape[0] for c in contours])
            pix = contours[cmax].astype(int).squeeze()
            if len(pix) > 4:
                outpix.append(pix)
            else:
                outpix.append(np.zeros((0, 2)))
    return outpix


#根据图片路径显示图片
def display_image():
    global img_width, img_height, im, img
    img = Image.open(photo_path)
    img_width, img_height = img.size
    img = img.resize((fig_width,fig_height))
    im = ImageTk.PhotoImage(img)
    cell_photo.config(image=im)
    cell_photo.update()
    label_photoReady.config(text='图片文件已载入')


#创建一个白色画布，用于图片路径为空时显示
def create_white_image():
    white_image = Image.new('RGB', (fig_width, fig_height), 'white')
    return white_image


#显示下一张图片功能
def display_next_img():
    global photo_path, image_number
    if image_number == len(image_files)-1:
        messagebox.showwarning("错误","已经是最后一张图片")
        return
    else:
        image_number = image_number + 1
        photo_path = image_files[image_number]
        display_image()
        find_matching_npy()


#显示上一张图片功能
def display_previous_img():
    global photo_path, image_number
    if image_number == 0:
        messagebox.showwarning("错误","已经是第一张图片")
        return
    else:
        image_number = image_number - 1
        photo_path = image_files[image_number]
        display_image()
        find_matching_npy()


#选择细胞图片文件夹功能，选择正确文件夹后可以记录里边jpg文件
def load_images_from_folder():
    global photo_path
    label_photoReady.config(text='图片文件未载入')
    folder_selected = filedialog.askdirectory(title='选择文件夹', initialdir='/')
    if folder_selected:
        global image_files, image_number
        files = os.listdir(folder_selected)
        image_files = [os.path.join(folder_selected, f) for f in files if f.lower().endswith(('.jpg'))]
        if len(image_files) == 0 :
            label_photoReady.config(text='没有有效的图片文件')
        else:
            photo_path = image_files[0]
            image_number = 0
            display_image()      


#选择细胞轮廓文件夹功能，选择正确文件夹后可以记录里边jpg文件
def load_contours_from_folder():
    global continues_folder_selected
    label_npyReady.config(text='轮廓文件未载入')
    continues_folder_selected = filedialog.askdirectory(title='选择文件夹', initialdir='/')
    global npy_files
    files = os.listdir(continues_folder_selected)
    npy_files = [os.path.join(continues_folder_selected, f) for f in files if f.lower().endswith(('.npy'))]
    if len(npy_files) == 0:
        label_npyReady.config(text='没有有效的轮廓文件')
    else:
        find_matching_npy()


#选择轮廓文件夹
def find_matching_npy():
        global outlines, dat, cell_type
        global seg_path; continues_folder_selected
        photo_path = image_files[image_number]
        file_name = os.path.splitext(photo_path)[0]
        expected_npy = f"{file_name}.npy"
        seg_path = os.path.join(continues_folder_selected, expected_npy)
        if os.path.exists(seg_path):
            label_npyReady.config(text='轮廓文件已载入')
            dat = np.load(seg_path, allow_pickle=True).item()
            outlines = outlines_list(dat['masks'])
            ncellcontours = len(outlines)
            label_cellCount.config(text='本张中细胞共计' + str(ncellcontours))
            cell_type = np.ones((1, ncellcontours))
        else:
           label_npyReady.config(text='未找到该图片匹配的轮廓文件')


#点击图片让该位置细胞高亮
def get_click_position(event):
    if seg_path == '':
        messagebox.showwarning("错误", "请选择轮廓文件")
        return
    else:
        global dat, cell_number
        x_coordinate = int(event.x*img_width/fig_width)
        y_coordinate = int(event.y*img_height/fig_height)
        cell_number = dat['masks'][y_coordinate,x_coordinate]
        label_cell()


#标记异常细胞
def label_bad_cell():
    if cell_number is not None and cell_number <= len(outlines):
        cell_type[0][cell_number-1] = 2
        on_checkbutton_change()
    else:
        messagebox.showwarning("错误", "请选择一个细胞")


#选择不是细胞
def label_not_cell():
    if cell_number is not None and cell_number <= len(outlines):
        cell_type[0][cell_number-1] = 0
        on_checkbutton_change()
    else:
        messagebox.showwarning("错误", "请选择一个细胞")


#防止误触还是做一个正常细胞的按钮
def label_normal_cell():
    if cell_number is not None and cell_number <= len(outlines):
        cell_type[0][cell_number-1] = 1
        on_checkbutton_change()
    else:
        messagebox.showwarning("错误", "请选择一个细胞")


class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.grid = ()
        self.create_widgets()

    def create_widgets(self):
        global im
        self.canvas = tk.Canvas(self, width=600, height=450)
        self.canvas.grid(row=2,column=4,rowspan=30)
        self.photo = im
        self.canvas.create_image(0, 0, anchor='nw', image=self.photo)

    def activate_drawing(self):
        self.canvas.bind('<Motion>', self.draw_lines)

    def draw_lines(self, event):
        self.canvas.delete('all')
        self.canvas.create_image(0, 0, anchor='nw', image=self.photo)
        self.canvas.create_line(0, event.y, self.canvas.winfo_width(), event.y, fill="blue")
        self.canvas.create_line(event.x, 0, event.x, self.canvas.winfo_height(), fill="red")



#统计细胞个数
if seg_path == '':
    ncellcontours = 0
else:
    dat = np.load(seg_path, allow_pickle=True).item()
    outlines = outlines_list(dat['masks'])
    ncellcontours = len(outlines)


# 保存数据
def save_cellnotes(noting_file):
    data = str(os.times()) + '\n'
    data.append(cell_type) + '\n'
    noting_file.write(str(cell_type))
    noting_file.close()



#初始化界面
root = tk.Tk()
root.title('细胞标记界面')
root.geometry('900x600')


#定义按钮
menu_bar = tk.Menu(root) # head menu
filemenu = tk.Menu(menu_bar, tearoff=False)
filemenu.add_command(label="选择细胞图片文件", command=lambda: load_images_from_folder())
filemenu.add_command(label="选择细胞轮廓文件", command=lambda: load_contours_from_folder())
filemenu.add_separator()
filemenu.add_command(label='保存细胞标记文件', command=lambda: save_cellnotes(noting_file))
filemenu.add_separator()
filemenu.add_command(label='退出', command=root.quit)
menu_bar.add_cascade(label='文件', menu=filemenu)

frame_photo = tk.Frame(root, width=600, height=450)



#显示图片
if photo_path:
    display_image()
else:
    white_img = create_white_image()
    img_resized = white_img.resize((fig_width,fig_height))
    photo_img = ImageTk.PhotoImage(img_resized)
    im = photo_img
    cell_photo = tk.Label(frame_photo, image=photo_img)




app = Application(master=frame_photo)
text_cellCount = '本张中细胞共计' + str(ncellcontours)
label_cellCount = tk.Label(root,text=text_cellCount)
label_photoReady = tk.Label(root,text='图片文件未载入')
label_npyReady = tk.Label(root,text='轮廓文件未载入')
button_bad= tk.Button(root,text='异常细胞',  command=label_bad_cell)
button_blank = tk.Button(root,text='不是细胞',command=label_not_cell)
button_normal = tk.Button(root,text='正常细胞',command=label_normal_cell)
checkbutton_showOutline = tk.IntVar()
checkbutton = tk.Checkbutton(root, text="显示轮廓", variable=checkbutton_showOutline, command=on_checkbutton_change)
# button_noteUnnoted = tk.Button(root,text='未标注的细胞', command=app.activate_drawing)
button_next = tk.Button(root,text='下一张',command = display_next_img)
button_previous = tk.Button(root,text='上一张',command = display_previous_img)



#按钮位置:
root.config(menu=menu_bar)
label_photoReady.grid(row=10,column=1,columnspan=2)
label_npyReady.grid(row=12,column=1,columnspan=2)
label_cellCount.grid(row=14,column=1,columnspan=2)
button_bad.grid(row=16,column=1,columnspan=2)
button_blank.grid(row=17,column=1,columnspan=2)
button_normal.grid(row=18,column=1,columnspan=2)
checkbutton.grid(row=20, column=1,columnspan=2, sticky='ew')
# button_noteUnnoted.grid(row=20,column=1,columnspan=2)
button_previous.grid(row=24,column=1)
button_next.grid(row=24,column=2)
label_blank = tk.Label(root,text='\t').grid(row=1,column=3)
label_blank = tk.Label(root,text='\t').grid(row=1,column=4)
frame_photo.grid(row=2,column=4,rowspan=30)
cell_photo.grid()
cell_photo.bind('<Button-1>', get_click_position)




##########
root.protocol("WM_DELETE_WINDOW", close_window)
root.resizable(0,0)
root.mainloop()